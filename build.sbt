organization := "com.kernelogic"

name := "play-redis"

version := "1.0.4-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

// match official Play plugins Scala version settings

scalaVersion := "2.11.8"

libraryDependencies ++= Seq(
  cache,
  "com.typesafe.play.modules" %% "play-modules-redis" % "2.4.1",
  "ru.pilin.redis.runner" % "redis-runner-junit" % "0.0.3-oschina" % "test"
)

resolvers ++= Seq(
  // For sedis
  // Mirror of http://pk11-scratch.googlecode.com/svn/trunk hosted by Kernelogic Inc
  "pk11-scratch" at "http://115.28.15.44:8080/pk11-scratch/",
  // For redis-runner
  "OS China 3rd Party" at "http://maven.oschina.net/content/repositories/thirdparty/"
)

javaOptions in Test ++= Seq(
  "-Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=9998",
  "-Xms512M",
  "-Xmx2048M",
  "-Xss1M",
  "-Dconfig.file=conf/application-test.conf")

testOptions += Tests.Argument(TestFrameworks.JUnit, "-v")
