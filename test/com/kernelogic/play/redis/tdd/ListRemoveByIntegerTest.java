package com.kernelogic.play.redis.tdd;

import org.junit.Assert;
import org.junit.Test;

import com.kernelogic.play.redis.Redis;
import com.kernelogic.play.redis.api.RedisListManager;
import com.kernelogic.play.redis.test.UnitTestWithRedis;

/**
 * {@link RedisListManager#removeAt(String, int)} should work as expected if an {@link Integer} is passed in to it.
 * 
 * @see <a href="https://bitbucket.org/kernelogicinc/play-redis/issue/4">Issue #4</a>
 * 
 * @author Peter Fu
 *
 * @since 1.0.1
 */
public class ListRemoveByIntegerTest extends UnitTestWithRedis {

    @Test
    public void test() {
        Redis.LIST.add("example", 1, 2, 3, 4);

        Integer index = 1; // note this is an Integer not an int
        Redis.LIST.removeAt("example", index);

        Assert.assertFalse("2 should be deleted form the list", Redis.LIST.contains("example", 2));
    }

}
