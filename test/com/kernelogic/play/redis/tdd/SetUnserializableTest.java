package com.kernelogic.play.redis.tdd;

import org.junit.Assert;
import org.junit.Test;

import com.kernelogic.play.redis.Redis;
import com.kernelogic.play.redis.test.UnitTestWithRedis;

/**
 * Try to store an unserializable object in Redis.
 * 
 * @see <a href="https://bitbucket.org/kernelogicinc/play-redis/issue/2">Issue #2</a>
 * 
 * @author Peter Fu
 * 
 * @since 1.0.1
 */
public class SetUnserializableTest extends UnitTestWithRedis {

    @Test
    public void test() {
        UnserializableBook book = new UnserializableBook(999L, "One Thousand and One Nights");

        // null should be stored in Redis in this case
        // TODO although this behavior has been documented, maybe it's better to raise a RuntimeException here rather than to save null silently?
        Redis.set("An unserializable book", book);

        Assert.assertNull("null should be stored in Redis if an unserializable object is passed to Redis API", Redis.get("An unserializable book"));
    }

}
