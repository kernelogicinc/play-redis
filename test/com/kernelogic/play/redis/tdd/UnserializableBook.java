package com.kernelogic.play.redis.tdd;

/**
 * A simple POJO which doesn't implement {@link java.io.Serializable}.
 * 
 * @author Peter Fu
 * 
 * @since 1.0.1
 */
public class UnserializableBook {

    private Long id;

    private String name;

    public UnserializableBook() {

    }

    public UnserializableBook(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
