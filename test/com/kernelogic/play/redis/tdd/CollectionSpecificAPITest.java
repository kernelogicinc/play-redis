package com.kernelogic.play.redis.tdd;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.kernelogic.play.redis.Redis;
import com.kernelogic.play.redis.test.UnitTestWithRedis;

/**
 * Collection specific API is added as a convenience way to call the API which supports <a
 * href="https://today.java.net/article/2004/04/13/java-tech-using-variable-arguments">Variable Type Arguments</a>.
 * 
 * @see <a href="https://bitbucket.org/kernelogicinc/play-redis/issue/5">Issue #5</a>
 * 
 * @author Peter Fu
 *
 * @since 1.0.1
 */
public class CollectionSpecificAPITest extends UnitTestWithRedis {

    @Test
    public void testSetAdd() {
        List<String> colors = new ArrayList<String>();
        colors.add("Black");
        colors.add("White");
        colors.add("Green");
        colors.add("Blue");
        colors.add("Red");

        Redis.SET.add("colors", colors);

        Assert.assertEquals(5, Redis.SET.size("colors"));
    }

    @Test
    public void testSetRemove() {
        Redis.SET.add("colors", "Black", "White", "Green", "Blue", "Red");

        List<String> colors = new ArrayList<String>();
        colors.add("Black");
        colors.add("White");
        colors.add("Green");
        colors.add("Blue");
        colors.add("Red");

        Redis.SET.remove("colors", colors);

        Assert.assertEquals(0, Redis.SET.size("colors"));
    }

    @Test
    public void testListAdd() {
        List<String> colors = new ArrayList<String>();
        colors.add("Black");
        colors.add("White");
        colors.add("Green");
        colors.add("Blue");
        colors.add("Red");

        Redis.LIST.add("colors", colors);

        Assert.assertEquals(5, Redis.LIST.size("colors"));
    }

    @Test
    public void testListAddFirst() {
        List<String> colors = new ArrayList<String>();
        colors.add("Black");
        colors.add("White");
        colors.add("Green");
        colors.add("Blue");
        colors.add("Red");

        Redis.LIST.addFirst("colors", colors);

        Assert.assertEquals(5, Redis.LIST.size("colors"));
    }

    @Test
    public void testListAddLast() {
        List<String> colors = new ArrayList<String>();
        colors.add("Black");
        colors.add("White");
        colors.add("Green");
        colors.add("Blue");
        colors.add("Red");

        Redis.LIST.addLast("colors", colors);

        Assert.assertEquals(5, Redis.LIST.size("colors"));
    }

}
