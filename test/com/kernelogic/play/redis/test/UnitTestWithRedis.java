package com.kernelogic.play.redis.test;

import static com.kernelogic.play.redis.test.Helpers.fakeRedisServerRunner;
import static play.test.Helpers.start;
import static play.test.Helpers.stop;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;

import play.Application;
import play.Mode;
import play.Play;
import play.inject.guice.GuiceApplicationBuilder;
import ru.pilin.redis.runner.core.RedisServerRunner;

/**
 * Presents a unit test class with redis.
 * 
 * @author Peter Fu
 * 
 * @since 1.0.0
 */
public abstract class UnitTestWithRedis {

    private Application application;

    private RedisServerRunner runner;

    @Before
    public void commonBefore() {
        application = new GuiceApplicationBuilder()
                .in(Mode.TEST)
                .build();

        start(application);

        // Need to make sure the folder referenced in conf/redis-test.conf exists
        Play.application().getFile("data-test/redis").mkdirs();

        runner = fakeRedisServerRunner();
        runner.start();
    }

    @After
    public void commonAfter() throws IOException {
        File dataTestFolder = Play.application().getFile("data-test");
        stop(application);
        runner.shutdown();

        FileUtils.forceDelete(dataTestFolder);
    }

    /**
     * A handful method to get rid of method level {@code @SuppressWarnings("unchecked")} annotations.
     * 
     * <p>
     * Copied from {@code com.kernelogic.process6.core.utils.Objects}.
     * </p>
     * 
     * <p>
     * Copied from http://www.whizu.org/articles/how-to-avoid-unchecked-cast-warnings-with-java-generics.whizu.
     * </p>
     * 
     * @param obj
     * @return
     * 
     * @author Rudy D'hauwe
     * 
     * @since 1.0.1
     */
    @SuppressWarnings("unchecked")
    protected static <T> T cast(Object obj) {
        return (T) obj;
    }

}
