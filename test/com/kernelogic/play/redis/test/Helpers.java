package com.kernelogic.play.redis.test;

import play.Logger;
import play.Play;
import ru.pilin.redis.runner.core.RedisServerRunner;

/**
 * Supplement to play.test.Helpers to provide Redis server support in test.
 * 
 * @author Peter Fu
 * 
 * @since 1.0.0
 */
public class Helpers {

    private static final Logger.ALogger LOGGER = Logger.of(Helpers.class);

    /**
     * Creates a fake Redis server based on the configurations.
     * 
     * <dl>
     * <dt>redis.host</dt>
     * <dd>Specified by play-plugins-redis, default as "localhost", normally don't need to change in test mode</dd>
     * <dt>redis.port</dt>
     * <dd>Specified by play-plugins-redis, default as 6379, normally changed to another port in test mode</dd>
     * <dt>redis.path</dt>
     * <dd>Specified by play-redis (this library), no default value, must be configured correctly to make it work in test mode</dd>
     * </dl>
     * 
     * @return
     * 
     * @since 1.0.0
     */
    public static RedisServerRunner fakeRedisServerRunner() {
        String redisHost = Play.application().configuration().getString("redis.host");
        int redisPort = Play.application().configuration().getInt("redis.port");
        String redisPath = Play.application().configuration().getString("redis.path");

        RedisServerRunner runner = new RedisServerRunner(redisHost, redisPort);
        // 从配置文件中获取redis的路径，然后设置到RedisServerRunner对象上
        runner.setRedisServerCmd(redisPath);

        LOGGER.debug("Prepared Reids server using the following configurations.");
        LOGGER.debug("path: {}", redisPath);
        LOGGER.debug("host: {}", redisHost);
        LOGGER.debug("port: {}", redisPort);

        return runner;
    }

}
