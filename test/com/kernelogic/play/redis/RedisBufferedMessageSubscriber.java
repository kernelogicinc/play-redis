package com.kernelogic.play.redis;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CountDownLatch;

import redis.clients.jedis.JedisPubSub;

/**
 * A Redis subscriber which stores the received messages in a buffered list.
 *
 * @author Peter Fu
 *
 * @since 1.0.1
 */
public class RedisBufferedMessageSubscriber extends JedisPubSub {

    /**
     * A count down latch to notify other threads that this subscriber is started.
     */
    private final CountDownLatch latch;

    // must use concurrent collection to make it thread safe
    private Queue<String> bufferedMessages = new ConcurrentLinkedQueue<String>();

    public RedisBufferedMessageSubscriber(CountDownLatch latch) {
        this.latch = latch;
    }

    @Override
    public void onMessage(String channel, String message) {
        bufferedMessages.add(message);
    }

    @Override
    public void onPMessage(String pattern, String channel, String message) {

    }

    @Override
    public void onSubscribe(String channel, int subscribedChannels) {
        latch.countDown();
    }

    @Override
    public void onUnsubscribe(String channel, int subscribedChannels) {

    }

    @Override
    public void onPUnsubscribe(String pattern, int subscribedChannels) {

    }

    @Override
    public void onPSubscribe(String pattern, int subscribedChannels) {

    }

    public Queue<String> getBufferedMessages() {
        return bufferedMessages;
    }

}
