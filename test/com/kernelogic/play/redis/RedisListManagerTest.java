package com.kernelogic.play.redis;

import org.junit.Assert;
import org.junit.Test;

import com.kernelogic.play.redis.test.UnitTestWithRedis;

/**
 * Tests for RedisListManager.
 * 
 * @author Lin Li
 * 
 * @since 1.0.1
 */
public class RedisListManagerTest extends UnitTestWithRedis {

    @Test
    public void basic() {
        // test add/get/getAs
        Redis.LIST.add("key", "1", "2", "3");
        Assert.assertEquals(3, Redis.LIST.size("key"));

        Assert.assertEquals("1", Redis.LIST.get("key", 0));
        Assert.assertEquals("2", Redis.LIST.get("key", 1));
        Assert.assertEquals("3", Redis.LIST.get("key", 2));

        Assert.assertEquals("1", Redis.LIST.getAs("key", 0, String.class));
        Assert.assertEquals("2", Redis.LIST.getAs("key", 1, String.class));
        Assert.assertEquals("3", Redis.LIST.getAs("key", 2, String.class));

        // test addLast
        Redis.LIST.addLast("key", "a", "b", "c");
        Assert.assertEquals(6, Redis.LIST.size("key"));

        Assert.assertEquals("1", Redis.LIST.getAs("key", 0, String.class));
        Assert.assertEquals("2", Redis.LIST.getAs("key", 1, String.class));
        Assert.assertEquals("3", Redis.LIST.getAs("key", 2, String.class));
        Assert.assertEquals("a", Redis.LIST.getAs("key", 3, String.class));
        Assert.assertEquals("b", Redis.LIST.getAs("key", 4, String.class));
        Assert.assertEquals("c", Redis.LIST.getAs("key", 5, String.class));

        // test addFirst
        Redis.LIST.addFirst("key", "d", "e", "f");
        Assert.assertEquals(9, Redis.LIST.size("key"));

        Assert.assertEquals("f", Redis.LIST.getAs("key", 0, String.class));
        Assert.assertEquals("e", Redis.LIST.getAs("key", 1, String.class));
        Assert.assertEquals("d", Redis.LIST.getAs("key", 2, String.class));
        Assert.assertEquals("1", Redis.LIST.getAs("key", 3, String.class));
        Assert.assertEquals("2", Redis.LIST.getAs("key", 4, String.class));
        Assert.assertEquals("3", Redis.LIST.getAs("key", 5, String.class));
        Assert.assertEquals("a", Redis.LIST.getAs("key", 6, String.class));
        Assert.assertEquals("b", Redis.LIST.getAs("key", 7, String.class));
        Assert.assertEquals("c", Redis.LIST.getAs("key", 8, String.class));

        // test getAs
        String[] strArray = {
            "f", "e", "d", "1", "2", "3", "a", "b", "c"
        };
        Assert.assertArrayEquals(strArray, Redis.LIST.getAs("key", String.class).toArray());

        // test getSubListAs
        Assert.assertArrayEquals(strArray, Redis.LIST.getSubListAs("key", 0, 9, String.class).toArray());

        // test remove/size/getAs
        Redis.LIST.removeAt("key", 1); // this should delete "e"
        Redis.LIST.remove("key", "d");

        Book book = new Book();
        book.setId(new Long(23));
        book.setName("bookTest");
        Redis.LIST.addFirst("key", book);

        Assert.assertEquals(8, Redis.LIST.size("key"));
        // FIXME This is not the right way to use the lib, so don't create a test case like this
        // Reason: since we're enforcing type safety, all elements in the List must be of the same type
        Assert.assertEquals(23, ((Book) Redis.LIST.getAs("key", 0, Book.class)).getId().intValue());
        Assert.assertEquals("bookTest", ((Book) Redis.LIST.getAs("key", 0, Book.class)).getName());
        Assert.assertEquals("f", Redis.LIST.getAs("key", 1, String.class));
        Assert.assertEquals("1", Redis.LIST.getAs("key", 2, String.class));
        Assert.assertEquals("2", Redis.LIST.getAs("key", 3, String.class));
        Assert.assertEquals("3", Redis.LIST.getAs("key", 4, String.class));
        Assert.assertEquals("a", Redis.LIST.getAs("key", 5, String.class));
        Assert.assertEquals("b", Redis.LIST.getAs("key", 6, String.class));
        Assert.assertEquals("c", Redis.LIST.getAs("key", 7, String.class));

        // test contains/remove
        Assert.assertFalse(Redis.LIST.contains("key", "d"));
        Assert.assertFalse(Redis.LIST.contains("key", "e"));
        Assert.assertTrue(Redis.LIST.contains("key", book));

        Redis.LIST.remove("key", book);
        Assert.assertFalse(Redis.LIST.contains("key", book));
        Assert.assertTrue(Redis.LIST.contains("key", "f"));
        Assert.assertTrue(Redis.LIST.contains("key", "1"));
        Assert.assertTrue(Redis.LIST.contains("key", "2"));
        Assert.assertTrue(Redis.LIST.contains("key", "3"));
        Assert.assertTrue(Redis.LIST.contains("key", "a"));
        Assert.assertTrue(Redis.LIST.contains("key", "b"));
        Assert.assertTrue(Redis.LIST.contains("key", "c"));
    }

}
