package com.kernelogic.play.redis;

import java.util.ArrayList;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

import com.kernelogic.play.redis.test.UnitTestWithRedis;

/**
 * Tests for RedisSetManager.
 * 
 * @author Peter Fu
 * 
 * @since 1.0.1
 */
public class RedisSetManagerTest extends UnitTestWithRedis {

    @Test
    public void basic() {
        Redis.SET.add("fruits", "Apple", "Pear", "Banana", "Peach");

        Assert.assertEquals(4, Redis.SET.size("fruits"));
        Assert.assertTrue(Redis.SET.contains("fruits", "Apple"));
        Assert.assertTrue(Redis.SET.contains("fruits", "Pear"));
        Assert.assertTrue(Redis.SET.contains("fruits", "Banana"));
        Assert.assertTrue(Redis.SET.contains("fruits", "Peach"));

        Set<String> fruitsFromRedis = Redis.SET.getAs("fruits", String.class);
        Assert.assertEquals(4, fruitsFromRedis.size());
        Assert.assertTrue(fruitsFromRedis.contains("Apple"));
        Assert.assertTrue(fruitsFromRedis.contains("Pear"));
        Assert.assertTrue(fruitsFromRedis.contains("Banana"));
        Assert.assertTrue(fruitsFromRedis.contains("Peach"));

        Redis.SET.remove("fruits", "Pear", "Peach");
        Assert.assertEquals(2, Redis.SET.size("fruits"));
        Assert.assertTrue(Redis.SET.contains("fruits", "Apple"));
        Assert.assertFalse(Redis.SET.contains("fruits", "Pear"));
        Assert.assertTrue(Redis.SET.contains("fruits", "Banana"));
        Assert.assertFalse(Redis.SET.contains("fruits", "Peach"));
    }

    @Test
    public void testAddOrRemoveNothing() {
        Redis.SET.add("fruits", "Apple", "Pear", "Banana", "Peach");
        Redis.SET.add("fruits", new ArrayList<String>());
        Assert.assertEquals(4, Redis.SET.size("fruits"));
        Redis.SET.remove("fruits", new ArrayList<String>());
        Assert.assertEquals(4, Redis.SET.size("fruits"));
    }

}
