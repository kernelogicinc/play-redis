package com.kernelogic.play.redis;

import org.junit.Assert;
import org.junit.Test;

import com.kernelogic.play.redis.test.UnitTestWithRedis;

/**
 * Tests for RedisObjectCaster.
 * 
 * @author Peter Fu
 * 
 * @since 1.0.1.1
 */
public class RedisObjectCasterTest extends UnitTestWithRedis {

    @Test
    public void testCasts() {
        Redis.set("testnull", null);
        Assert.assertNull(Redis.getAs("testnull", Boolean.class));
        Assert.assertNull(Redis.getAs("testnull", String.class));
        Assert.assertNull(Redis.getAs("testnull", Integer.class));
        Assert.assertNull(Redis.getAs("testnull", Long.class));

        Redis.set("test1", "true");
        Assert.assertEquals("true", Redis.getAs("test1", String.class));
        Assert.assertEquals(Boolean.TRUE, Redis.getAs("test1", Boolean.class));

        Redis.set("test2", "false");
        Assert.assertEquals("false", Redis.getAs("test2", String.class));
        Assert.assertEquals(Boolean.FALSE, Redis.getAs("test2", Boolean.class));

        Redis.set("test3", 100);
        Assert.assertEquals("100", Redis.getAs("test3", String.class));
        Assert.assertEquals(Integer.valueOf(100), Redis.getAs("test3", Integer.class));
        Assert.assertEquals(Long.valueOf(100L), Redis.getAs("test3", Long.class));

        Redis.set("test4", 100L);
        Assert.assertEquals("100", Redis.getAs("test4", String.class));
        Assert.assertEquals(Integer.valueOf(100), Redis.getAs("test4", Integer.class));
        Assert.assertEquals(Long.valueOf(100L), Redis.getAs("test4", Long.class));
    }

}
