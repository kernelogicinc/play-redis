package com.kernelogic.play.redis;

import java.io.Serializable;

/**
 * A simple POJO used in tests.
 * 
 * @author Peter Fu
 * 
 * @since 1.0.1
 */
public class Book implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private String name;

    public Book() {

    }

    public Book(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
