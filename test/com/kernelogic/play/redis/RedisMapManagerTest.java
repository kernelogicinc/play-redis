package com.kernelogic.play.redis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

import com.kernelogic.play.redis.test.UnitTestWithRedis;

/**
 * Tests for RedisSetManager.
 * 
 * @author Peter Fu
 * 
 * @since 1.0.1
 */
public class RedisMapManagerTest extends UnitTestWithRedis {

    @Test
    public void basic() {
        // put in some initial data
        Redis.MAP.put("country-map", "cn", "China");
        Redis.MAP.put("country-map", "us", "America");
        Redis.MAP.put("country-map", "en", "English");

        Assert.assertEquals(3, Redis.MAP.size("country-map"));

        Assert.assertEquals("China", Redis.MAP.get("country-map", "cn"));
        Assert.assertEquals("America", Redis.MAP.get("country-map", "us"));
        Assert.assertEquals("English", Redis.MAP.get("country-map", "en"));

        Assert.assertEquals("China", Redis.MAP.getAs("country-map", "cn", String.class));
        Assert.assertEquals("America", Redis.MAP.getAs("country-map", "us", String.class));
        Assert.assertEquals("English", Redis.MAP.getAs("country-map", "en", String.class));

        Assert.assertTrue(Redis.MAP.containsKey("country-map", "cn"));
        Assert.assertTrue(Redis.MAP.containsKey("country-map", "us"));
        Assert.assertTrue(Redis.MAP.containsKey("country-map", "en"));

        // add jp
        Redis.MAP.put("country-map", "jp", "Japan");

        Assert.assertEquals(4, Redis.MAP.size("country-map"));
        Assert.assertEquals("China", Redis.MAP.getAs("country-map", "cn", String.class));
        Assert.assertEquals("America", Redis.MAP.getAs("country-map", "us", String.class));
        Assert.assertEquals("English", Redis.MAP.getAs("country-map", "en", String.class));
        Assert.assertEquals("Japan", Redis.MAP.getAs("country-map", "jp", String.class));

        // remove us and jp
        Redis.MAP.remove("country-map", "us", "jp");

        Assert.assertEquals(2, Redis.MAP.size("country-map"));
        Assert.assertEquals("China", Redis.MAP.getAs("country-map", "cn", String.class));
        Assert.assertEquals("English", Redis.MAP.getAs("country-map", "en", String.class));

        Assert.assertTrue(Redis.MAP.containsKey("country-map", "cn"));
        Assert.assertFalse(Redis.MAP.containsKey("country-map", "us"));
        Assert.assertTrue(Redis.MAP.containsKey("country-map", "en"));
        Assert.assertFalse(Redis.MAP.containsKey("country-map", "jp"));

        // API putAll() only supports Map<String, Object>, which makes it less useful
        Map<String, Object> brics = new HashMap<String, Object>();
        brics.put("bra", "Brazil");
        brics.put("ru", "Russia");
        brics.put("in", "India");
        brics.put("cn", "China"); // note this already exists in Redis, so it won't be added twice

        // add BRICs
        Redis.MAP.putAll("country-map", brics);

        Assert.assertEquals(5, Redis.MAP.size("country-map"));
        Assert.assertEquals("China", Redis.MAP.getAs("country-map", "cn", String.class));
        Assert.assertEquals("English", Redis.MAP.getAs("country-map", "en", String.class));
        Assert.assertEquals("Brazil", Redis.MAP.getAs("country-map", "bra", String.class));
        Assert.assertEquals("Russia", Redis.MAP.getAs("country-map", "ru", String.class));
        Assert.assertEquals("India", Redis.MAP.getAs("country-map", "in", String.class));

        Map<String, String> countryMapFromRedis = Redis.MAP.getAs("country-map", String.class);
        Assert.assertEquals(5, countryMapFromRedis.size());
        Assert.assertEquals("China", countryMapFromRedis.get("cn"));
        Assert.assertEquals("English", countryMapFromRedis.get("en"));
        Assert.assertEquals("Brazil", countryMapFromRedis.get("bra"));
        Assert.assertEquals("Russia", countryMapFromRedis.get("ru"));
        Assert.assertEquals("India", countryMapFromRedis.get("in"));

        List<String> bricsKeys = new ArrayList<String>();
        bricsKeys.add("bra");
        bricsKeys.add("ru");
        bricsKeys.add("in");
        bricsKeys.add("cn");

        Map<String, String> bricsFromRedis = Redis.MAP.getSubMapAs("country-map", bricsKeys, String.class);
        Assert.assertEquals(4, bricsFromRedis.size());
        Assert.assertEquals("Brazil", bricsFromRedis.get("bra"));
        Assert.assertEquals("Russia", bricsFromRedis.get("ru"));
        Assert.assertEquals("India", bricsFromRedis.get("in"));
        Assert.assertEquals("China", bricsFromRedis.get("cn"));

        // API values() is not type safe
        List<Object> countryNameObjectsFromRedis = Redis.MAP.values("country-map");
        Assert.assertEquals(5, countryNameObjectsFromRedis.size());
        Assert.assertTrue(countryNameObjectsFromRedis.contains("China"));
        Assert.assertTrue(countryNameObjectsFromRedis.contains("English"));
        Assert.assertTrue(countryNameObjectsFromRedis.contains("Brazil"));
        Assert.assertTrue(countryNameObjectsFromRedis.contains("Russia"));
        Assert.assertTrue(countryNameObjectsFromRedis.contains("India"));

        List<String> countryNamessFromRedis = Redis.MAP.valuesAs("country-map", String.class);
        Assert.assertEquals(5, countryNamessFromRedis.size());
        Assert.assertTrue(countryNamessFromRedis.contains("China"));
        Assert.assertTrue(countryNamessFromRedis.contains("English"));
        Assert.assertTrue(countryNamessFromRedis.contains("Brazil"));
        Assert.assertTrue(countryNamessFromRedis.contains("Russia"));
        Assert.assertTrue(countryNamessFromRedis.contains("India"));

        // remove BRICs
        Redis.MAP.remove("country-map", brics.keySet());
        Set<String> countryKeysFromRedis = Redis.MAP.keySet("country-map");
        Assert.assertEquals(1, countryKeysFromRedis.size());
        Assert.assertTrue(countryNamessFromRedis.contains("English"));
    }

}
