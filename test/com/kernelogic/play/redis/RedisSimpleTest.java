package com.kernelogic.play.redis;

import java.util.Date;

import org.junit.Assert;
import org.junit.Test;

import com.kernelogic.play.redis.test.UnitTestWithRedis;

/**
 * A simple test.
 * 
 * @author Peter Fu
 * 
 * @since 1.0.0
 */
public class RedisSimpleTest extends UnitTestWithRedis {

    @Test
    public void basic() {
        // 测试redis是否可以连接成功并且可以写入值
        Redis.set("test-redis-can-work", "some string");
        Assert.assertEquals("some string", Redis.get("test-redis-can-work"));

        Redis.set("test-redis-can-work", true);
        Assert.assertTrue((Boolean) Redis.getAs("test-redis-can-work", Boolean.class));

        Redis.set("test-redis-can-work", false);
        Assert.assertFalse((Boolean) Redis.getAs("test-redis-can-work", Boolean.class));

        Redis.set("test-redis-can-work", null);
        Assert.assertNull(Redis.get("test-redis-can-work"));

        Redis.set("test-redis-can-work", 12345);
        Assert.assertEquals(new Integer(12345), Redis.getAs("test-redis-can-work", Integer.class));

        Redis.set("test-redis-can-work", 12345L);
        Assert.assertEquals(new Long(12345), Redis.getAs("test-redis-can-work", Long.class));

        Date now = new Date();
        Redis.set("test-redis-can-work", now);
        Assert.assertEquals(now, Redis.getAs("test-redis-can-work", Date.class));

        // 测试成功，删除数据
        Redis.remove("test-redis-can-work");
        Assert.assertNull(Redis.get("test-redis-can-work"));
    }

}
