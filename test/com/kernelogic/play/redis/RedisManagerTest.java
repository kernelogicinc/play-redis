package com.kernelogic.play.redis;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CountDownLatch;

import org.junit.Assert;
import org.junit.Test;

import com.kernelogic.play.redis.test.UnitTestWithRedis;

/**
 * Tests for RedisManager.
 * 
 * @author Peter Fu
 * 
 * @since 1.0.1
 */
public class RedisManagerTest extends UnitTestWithRedis {

    @Test
    public void testSetGet() {
        // set/get a String
        Redis.set("Obi-Wan Kenobi", "May the Force be with you.");
        Assert.assertEquals("May the Force be with you.", Redis.get("Obi-Wan Kenobi"));
        Assert.assertEquals("May the Force be with you.", Redis.getAs("Obi-Wan Kenobi", String.class));

        // set/get an Integer
        Redis.set("TEN", new Integer(10));
        Assert.assertEquals(new Integer(10), Redis.get("TEN"));
        Assert.assertEquals(new Integer(10), Redis.getAs("TEN", Integer.class));

        // set/get an int
        Redis.set("THOUTHAND", 1000);
        Assert.assertEquals(1000, Redis.get("THOUTHAND"));
        Assert.assertEquals(1000, Redis.getAs("THOUTHAND", Integer.class).intValue());

        // set/get a Long
        Redis.set("PI*10^16", new Long(31415926535897932L));
        Assert.assertEquals(new Long(31415926535897932L), Redis.get("PI*10^16"));
        Assert.assertEquals(new Long(31415926535897932L), Redis.getAs("PI*10^16", Long.class));

        // set/get an long
        Redis.set("PI*10^18", 3141592653589793238L);
        Assert.assertEquals(3141592653589793238L, Redis.get("PI*10^18"));
        Assert.assertEquals(3141592653589793238L, Redis.getAs("PI*10^18", Long.class).longValue());

        // set/get an Float
        Redis.set("PI in Float", new Float(3.14F));
        Assert.assertEquals(new Float(3.14F), Redis.get("PI in Float"));
        Assert.assertEquals(new Float(3.14F), Redis.getAs("PI in Float", Float.class));

        // set/get an float
        Redis.set("PI in float", 3.14f);
        Assert.assertEquals(3.14f, Redis.get("PI in float"));
        Assert.assertEquals(3.14f, Redis.getAs("PI in float", Float.class).floatValue(), 0.0001F);

        // set/get an Double
        Redis.set("PI in Double", new Double(3.1415926535897932384626));
        Assert.assertEquals(new Double(3.1415926535897932384626), Redis.get("PI in Double"));
        Assert.assertEquals(new Double(3.1415926535897932384626), Redis.getAs("PI in Double", Double.class));

        // set/get an double
        Redis.set("PI in double", 3.1415926535897932384626);
        Assert.assertEquals(3.1415926535897932384626, Redis.get("PI in double"));
        Assert.assertEquals(3.1415926535897932384626, Redis.getAs("PI in Double", Double.class).doubleValue(), 0.000000000000000000000001);

        // set/get a Boolean
        Redis.set("The day after tomorrow is yesterday.", new Boolean(false));
        Assert.assertEquals(new Boolean(false), Redis.get("The day after tomorrow is yesterday."));
        Assert.assertEquals(new Boolean(false), Redis.getAs("The day after tomorrow is yesterday.", Boolean.class));

        // set/get a boolean
        Redis.set("1+1=2", true);
        Assert.assertEquals(new Boolean(false), Redis.get("The day after tomorrow is yesterday."));
        Assert.assertEquals(false, Redis.getAs("The day after tomorrow is yesterday.", Boolean.class).booleanValue());

        // set/get a Date
        Date date = new Date();
        Redis.set("Current Date (java.util.Date)", date);
        Assert.assertEquals(date, Redis.get("Current Date (java.util.Date)"));
        Assert.assertEquals(date, Redis.getAs("Current Date (java.util.Date)", Date.class));

        // set/get a POJO
        Book book = new Book(1L, "Thinking in Java");
        Redis.set("A famous java book", book);
        Book bookFromRedis = Redis.getAs("A famous java book", Book.class);
        Assert.assertEquals(book.getId(), bookFromRedis.getId());
        Assert.assertEquals(book.getName(), bookFromRedis.getName());

        // set/get a List of POJO
        Book book1 = new Book(101L, "Java Programming Language");
        Book book2 = new Book(202L, "Head First Java");
        Book book3 = new Book(303L, "Thinking in Java");
        List<Book> books = new ArrayList<Book>();
        books.add(book1);
        books.add(book2);
        books.add(book3);
        Redis.set("My favorite books", books);
        List<Book> booksFromRedis = cast(Redis.getAs("My favorite books", List.class));
        Assert.assertEquals(books.size(), booksFromRedis.size());
        Book book1FromRedis = booksFromRedis.get(0);
        Book book2FromRedis = booksFromRedis.get(1);
        Book book3FromRedis = booksFromRedis.get(2);
        Assert.assertEquals(book1.getId(), book1FromRedis.getId());
        Assert.assertEquals(book1.getName(), book1FromRedis.getName());
        Assert.assertEquals(book2.getId(), book2FromRedis.getId());
        Assert.assertEquals(book2.getName(), book2FromRedis.getName());
        Assert.assertEquals(book3.getId(), book3FromRedis.getId());
        Assert.assertEquals(book3.getName(), book3FromRedis.getName());

        // set/get null
        Redis.set("Nothing here", null);
        Assert.assertEquals(null, Redis.get("Nothing here"));
        Assert.assertEquals(null, Redis.getAs("Nothing here", Book.class));
    }

    @Test
    public void testSetWithExpire() throws InterruptedException {
        // the timing here is not 100% accurate according to the system threading, but should be good enough for the test case here
        Redis.set("I'm dispearing in 1 second", "Here I am!", 1);
        Assert.assertEquals("The value should exist if we get it immediately after set it", "Here I am!", Redis.getAs("I'm dispearing in 1 second", String.class));
        Thread.sleep(500L); // after 500ms the value should still exist
        Assert.assertEquals("The value should still exist after 500ms - the timing is not 100% accurate though (but should work)", "Here I am!",
                Redis.getAs("I'm dispearing in 1 second", String.class));
        Thread.sleep(1100L); // after 1100ms the value should be gone
        Assert.assertEquals("The value should be gone after 1100ms", null, Redis.getAs("I'm dispearing in 1 second", String.class));
    }

    @Test
    public void testKeyOperations() {
        Redis.set("p:1:foo", 1);
        Redis.set("p:1:bar", 2);
        Redis.set("p:1:baz", 3);
        Redis.set("p:1:qux", 4);
        Redis.set("p:2:foo", 5);
        Redis.set("p:2:bar", 6);
        Redis.set("p:2:baz", 7);
        Redis.set("p:2:qux", 8);

        Assert.assertEquals("There should be 4 keys staring with p:1:", 4, Redis.keys("p:1:*").size());
        Assert.assertEquals("There should be 4 keys staring with p:2:", 4, Redis.keys("p:2:*").size());
        Assert.assertEquals("There should be 8 keys in Redis in total", 8, Redis.keys("*").size());

        Set<String> p1Keys = Redis.keys("p:1:*");
        Assert.assertTrue(p1Keys.contains("p:1:foo"));
        Assert.assertTrue(p1Keys.contains("p:1:bar"));
        Assert.assertTrue(p1Keys.contains("p:1:baz"));
        Assert.assertTrue(p1Keys.contains("p:1:qux"));

        // remove one key from p2
        Redis.remove("p:2:baz");

        Assert.assertEquals("There should be 4 keys staring with p:1:", 4, Redis.keys("p:1:*").size());
        Assert.assertEquals("There should be 3 keys staring with p:2:", 3, Redis.keys("p:2:*").size());
        Assert.assertEquals("There should be 7 keys in Redis in total", 7, Redis.keys("*").size());

        Set<String> p2Keys = Redis.keys("p:2:*");
        Assert.assertTrue(p2Keys.contains("p:2:foo"));
        Assert.assertTrue(p2Keys.contains("p:2:bar"));
        Assert.assertFalse("p:2:baz should be removed already", p2Keys.contains("p:2:baz"));
        Assert.assertTrue(p2Keys.contains("p:2:qux"));

        // remove all p1 keys - regex not supported here - either list all keys just like below or use Redis.remove(Redis.keys("p:1:*"))
        Redis.remove("p:1:foo", "p:1:bar", "p:1:baz", "p:1:qux");

        Assert.assertEquals("There should be 0 keys staring with p:1:", 0, Redis.keys("p:1:*").size());
        Assert.assertEquals("There should be 3 keys staring with p:2:", 3, Redis.keys("p:2:*").size());
        Assert.assertEquals("There should be 3 keys in Redis in total", 3, Redis.keys("*").size());

        // remove keys by an empty Collection
        Redis.remove(new ArrayList<String>());
        Assert.assertEquals("No keys should be removed if an empty Collection is passed to Redis.remove()", 3, Redis.keys("*").size());

        // remove keys by an empty Array
        Redis.remove(new String[0]);
        Assert.assertEquals("No keys should be removed if an empty Array is passed to Redis.remove()", 3, Redis.keys("*").size());
    }

    @Test
    public void testPubSub() throws InterruptedException {
        // prepare some subscribers and start them

        // the latch here is used to make sure all the subscribers are active
        CountDownLatch latch = new CountDownLatch(3);

        final RedisBufferedMessageSubscriber foo = new RedisBufferedMessageSubscriber(latch);
        final RedisBufferedMessageSubscriber bar = new RedisBufferedMessageSubscriber(latch);
        final RedisBufferedMessageSubscriber foobar = new RedisBufferedMessageSubscriber(latch);

        new Thread() {
            public void run() {
                Redis.subscribe(foo, "channel-foo");
            };
        }.start();

        new Thread() {
            public void run() {
                Redis.subscribe(bar, "channel-bar");
            };
        }.start();

        new Thread() {
            public void run() {
                Redis.subscribe(foobar, "channel-foo", "channel-bar");
            };
        }.start();

        // wait until all the subscribers are subscribed
        latch.await();

        Redis.publish("channel-foo", "He has Van Gogh's ear for music.");
        Thread.sleep(100L); // just sleep a enough time for the message be received

        Assert.assertEquals("Subscriber foo should receive 1 message so far", 1, foo.getBufferedMessages().size());
        Assert.assertEquals("Subscriber bar should receive 0 message so far", 0, bar.getBufferedMessages().size());
        Assert.assertEquals("Subscriber foobar should receive 1 message so far", 1, foobar.getBufferedMessages().size());

        Redis.publish("channel-bar", "Why do you sit there looking like an envelope without any address on it?");
        Thread.sleep(100L);

        Assert.assertEquals("Subscriber foo should receive 1 message so far", 1, foo.getBufferedMessages().size());
        Assert.assertEquals("Subscriber bar should receive 1 message so far", 1, bar.getBufferedMessages().size());
        Assert.assertEquals("Subscriber foobar should receive 2 messages so far", 2, foobar.getBufferedMessages().size());

        Redis.publish("channel-foo", "I didn't attend the funeral, but I sent a nice letter saying I approved of it.");
        Thread.sleep(100L);

        Assert.assertEquals("Subscriber foo should receive 2 messages so far", 2, foo.getBufferedMessages().size());
        Assert.assertEquals("Subscriber bar should receive 1 message so far", 1, bar.getBufferedMessages().size());
        Assert.assertEquals("Subscriber foobar should receive 3 messages so far", 3, foobar.getBufferedMessages().size());

        Redis.publish("channel-bar", "A modest little person, with much to be modest about.");
        Thread.sleep(100L);

        Assert.assertEquals("Subscriber foo should receive 2 messages so far", 2, foo.getBufferedMessages().size());
        Assert.assertEquals("Subscriber bar should receive 2 messages so far", 2, bar.getBufferedMessages().size());
        Assert.assertEquals("Subscriber foobar should receive 4 messages so far", 4, foobar.getBufferedMessages().size());

        // check the messages content

        Assert.assertEquals("He has Van Gogh's ear for music.", foo.getBufferedMessages().poll());
        Assert.assertEquals("I didn't attend the funeral, but I sent a nice letter saying I approved of it.", foo.getBufferedMessages().poll());

        Assert.assertEquals("Why do you sit there looking like an envelope without any address on it?", bar.getBufferedMessages().poll());
        Assert.assertEquals("A modest little person, with much to be modest about.", bar.getBufferedMessages().poll());

        Assert.assertEquals("He has Van Gogh's ear for music.", foobar.getBufferedMessages().poll());
        Assert.assertEquals("Why do you sit there looking like an envelope without any address on it?", foobar.getBufferedMessages().poll());
        Assert.assertEquals("I didn't attend the funeral, but I sent a nice letter saying I approved of it.", foobar.getBufferedMessages().poll());
        Assert.assertEquals("A modest little person, with much to be modest about.", foobar.getBufferedMessages().poll());

        // always good to unsubscribe the subscribers before the application is shutdown
        foo.unsubscribe();
        bar.unsubscribe();
        foobar.unsubscribe();
    }

}
