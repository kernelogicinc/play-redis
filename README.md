# Play Redis #

Redis API based on Play! Framework 2 (from 2.2.x), its Redis plugin and Jedis with object support, type safety and connection management.

# Features #

- Object support and Type safety

    While all values stored in Redis are `String`, this library provides the ability to store Java objects via serialization (serialized objects are still readable in Redis for primitive types).

- Connection management

    With this library taking care of acquiring and releasing the Jedis connections (resources), the code to operate Redis would be much shorter.

# Requirements #

- play-redis >= 1.0.3 - JDK 8 is required

- play-redis <= 1.0.2 - JDK 7 is required because this library is compiled with JDK 7.

    Although most of the source code is compatible with JDK 6, it's still a reasonable amount of extra effort to make the distribution under `-source 1.6 -target 1.6`. With only JDK 7 installed the one who make the build will receive `warning: [options] bootstrap class path not set in conjunction with -source 1.6`, and the only way to get rid of it is to install JDK 6 and follow the [Cross-Compilation Example](http://docs.oracle.com/javase/7/docs/technotes/tools/solaris/javac.html#crosscomp-example) provided by Oracle, which we don't have time for.

# Usage #

- Add the library to your dependences.

For Play 2.4.x

```
libraryDependencies += "com.kernelogic" %% "play-redis" % "1.0.3"
```

For Play 2.3.x

```
libraryDependencies += "com.kernelogic" %% "play-redis" % "1.0.2"
```

For Play 2.2.x

```
libraryDependencies += "com.kernelogic" %% "play-redis" % "1.0.1"
```

- Register the repository resolver.

```
resolvers += "Kernelogic Public" at "http://115.28.15.44:8080/nexus/content/groups/public/"
```

- Since the repository is private, the following line is needed to set the password.

Please contact [Peter Fu](mailto:peter.fu@kernelogic.ca) for the password of the `public` account.

```
credentials in ThisBuild += Credentials("Sonatype Nexus Repository Manager", "115.28.15.44", "public", "CONTACT US FOR THE PASSWORD")
```

# Versions #

play-redis|Play! Framework|play-plugins-redis|Scala
---|---|---|---
1.0.3|2.4.x|2.4.1|2.11
1.0.2|2.3.x|2.3.1|2.10 and 2.11
1.0.1|2.2.x|2.2.1|2.10
1.0.0|2.2.x|2.2.0|2.10

# Supported Redis operations #

- String

    Wrapped as `Object` instead of `String`.

- List

    Wrapped as `List<Object>` instead of `List<String>`.

- Set

    Wrapped as `Set<Object>` instead of `Set<String>`.

- Map (Hash)

    Wrapped as `Map<String, Object>` instead of `Map<String, String>`.

- Pub/Sub

# Unsupported Redis operations/features

To make use of the unsupported Redis features, please submit a request to this library or use Jedis directly.

- SortedSet
- Pipeline
- Transaction

# Note #

- In Redis, index of List is `long`; while in Java index of `List` is `int`. This library would use `int` to make use of java `List` API.

- In Redis, size of Set, List or Map is `long`; while in Java size of `Collection` is `int`. This library would use `int` to make use of java `Collection` API.

- String type value is also changed after serialization by prefixing `"STR:"`. This is required because of object support feature. Otherwise, we can't identify the target class when deserialize a value (for example, the string value might start with `"BOL:"` or `"B64:"` itself).

- Please note objects passed to Redis API must implement `java.io.Serializable`, otherwise `null` will be stored in Redis silently (error is logged, but no exception is raised).

# Changelog #

## 1.0.3 ##

Improvements:

- Issue #8 Play 2.4 support

## 1.0.2.1 ##

Improvements:

- Issue #13 Tolerant type mismatch of basic types for getAs

## 1.0.2 ##

Improvements:

- Issue #7 Play 2.3 support

## 1.0.1.1 ##

Improvements:

- Issue #13 Tolerant type mismatch of basic types for getAs

## 1.0.1 ##

Improvements:

- Upgraded play-plugins-redis from 2.2.0 to 2.2.1
- Issue #1 Write unit tests for Redis API
- Issue #4 [BREAKING CHANGE|TDD] Better method name to remove an element from a List by index
- Code fully tested (code coverage 96%)

Bugs fixed:

- Issue #2 [TDD] Exception occurs when trying to retrieve an object which doesn't implement `java.io.Serializable` from Redis
- Issue #5 [TDD] `Collection` specific API fails to work as expected

## 1.0.0 ##

Initial release

# Publish guide #

## Update README ##

When listing issues, please follow the existing pattern. One thing to notice is must change something like `[FOO] [BAR]` to `[FOO|BAR]` otherwise Eclipse will complain some wikitext validation error (for example Issue #4).

## Cross-building for Play 2.3 ##

Since we are cross-building this library for both Scala 2.11 and 2.10 for Play 2.3, must use `+publish` instead of `publish` when make the distribution, otherwise only the version of Scala 2.11 will be built against.

# License #

This software is licensed under the Apache 2 license, quoted below.

Copyright (C) 2014-2016 Kernelogic Software Inc.

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this project except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0.

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.