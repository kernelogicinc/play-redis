resolvers += "Typesafe repository" at "https://repo.typesafe.com/typesafe/releases/"

// The Play plugin
addSbtPlugin("com.typesafe.play" % "sbt-plugin" % "2.4.0")

// Play requires sbteclipse 4.0.0 or newer.
// https://www.playframework.com/documentation/2.4.x/IDE#Setup-sbteclipse
addSbtPlugin("com.typesafe.sbteclipse" % "sbteclipse-plugin" % "4.0.0")

addSbtPlugin("de.johoop" % "jacoco4sbt" % "2.1.6")
