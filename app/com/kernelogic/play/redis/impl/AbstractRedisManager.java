package com.kernelogic.play.redis.impl;

import com.kernelogic.play.redis.internal.RedisSerializer;
import com.kernelogic.play.redis.internal.RedisSerializerFactory;
import com.kernelogic.play.redis.internal.RedisTemplate;

/**
 * Parent class of all Redis manager implementation classes.
 * 
 * @author Peter Fu
 * 
 * @since 1.0.0
 */
public abstract class AbstractRedisManager {

    protected RedisTemplate template = RedisTemplate.get();

    private RedisSerializer serializer = RedisSerializerFactory.get();

    protected String serialize(Object object) {
        return serializer.serialize(object);
    }

    protected String[] serialize(Object... objects) {
        String[] result = new String[objects.length];
        for (int i = 0; i < objects.length; i++) {
            result[i] = serializer.serialize(objects[i]);
        }
        return result;
    }

    protected Object deserialize(String string) {
        return serializer.deserialize(string);
    }

}
