package com.kernelogic.play.redis.impl;

import java.util.Collection;
import java.util.Set;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPubSub;

import com.kernelogic.play.redis.api.RedisManager;
import com.kernelogic.play.redis.internal.RedisCallback;
import com.kernelogic.play.redis.internal.RedisObjectCaster;

/**
 * Implementation of Redis common operations.
 * 
 * @author Fei Yan
 * 
 * @since 1.0.0
 */
public class RedisManagerImpl extends AbstractRedisManager implements RedisManager {

    @Override
    public void set(final String key, final Object value) {
        template.execute(new RedisCallback<Object>() {

            public Object doWithJedis(Jedis jedis) throws Exception {
                jedis.set(key, serialize(value));
                return null;
            }

        });
    }

    @Override
    public void set(final String key, final Object value, final int expire) {
        template.execute(new RedisCallback<Object>() {

            public Object doWithJedis(Jedis jedis) throws Exception {
                jedis.setex(key, expire, serialize(value));
                return null;
            }

        });

    }

    @Override
    public Object get(final String key) {
        return template.execute(new RedisCallback<Object>() {

            public Object doWithJedis(Jedis jedis) throws Exception {
                return deserialize(jedis.get(key));
            }

        });
    }

    @Override
    public <T> T getAs(final String key, final Class<T> clazz) {
        return template.execute(new RedisCallback<T>() {

            public T doWithJedis(Jedis jedis) throws Exception {
                return RedisObjectCaster.cast(deserialize(jedis.get(key)), clazz);
            }

        });
    }

    @Override
    public Set<String> keys(final String pattern) {
        return template.execute(new RedisCallback<Set<String>>() {

            @Override
            public Set<String> doWithJedis(Jedis jedis) throws Exception {
                return jedis.keys(pattern);
            }

        });
    }

    @Override
    public void remove(final String... keys) {
        if (keys.length == 0) { // Jedis doesn't accept empty array
            return;
        }

        template.execute(new RedisCallback<Object>() {

            public Object doWithJedis(Jedis jedis) throws Exception {
                jedis.del(keys);
                return null;
            }

        });
    }

    @Override
    public void remove(Collection<String> keys) {
        remove(keys.toArray(new String[0]));
    }

    @Override
    public void subscribe(final JedisPubSub jedisPubSub, final String... channels) {
        template.execute(new RedisCallback<Object>() {

            public Object doWithJedis(Jedis jedis) throws Exception {
                jedis.subscribe(jedisPubSub, channels);
                return null;
            }

        });
    }

    @Override
    public void publish(final String channel, final String message) {
        template.execute(new RedisCallback<Object>() {

            public Object doWithJedis(Jedis jedis) throws Exception {
                jedis.publish(channel, message);
                return null;
            }

        });
    }

}
