package com.kernelogic.play.redis.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import redis.clients.jedis.Jedis;

import com.kernelogic.play.redis.api.RedisListManager;
import com.kernelogic.play.redis.internal.RedisCallback;
import com.kernelogic.play.redis.internal.RedisObjectCaster;

/**
 * Implementation of Redis List operations.
 * 
 * @author Lin Li
 *
 * @since 1.0.0
 */
public class RedisListManagerImpl extends AbstractRedisManager implements RedisListManager {

    private static final String DELETED_PLACEHOLDER = "-- DELETED --";

    @Override
    public void add(String key, Object... values) {
        this.addLast(key, values);
    }

    @Override
    public void addLast(final String key, final Object... values) {
        template.execute(new RedisCallback<Object>() {

            @Override
            public Object doWithJedis(Jedis jedis) throws Exception {
                jedis.rpush(key, serialize(values));
                return null;
            }

        });
    }

    @Override
    public void addFirst(final String key, final Object... values) {
        template.execute(new RedisCallback<Object>() {

            @Override
            public Object doWithJedis(Jedis jedis) throws Exception {
                jedis.lpush(key, serialize(values));
                return null;
            }

        });
    }

    @Override
    public <T> void add(String key, Collection<T> values) {
        this.add(key, values.toArray());
    }

    @Override
    public <T> void addLast(String key, Collection<T> values) {
        this.addLast(key, values.toArray());
    }

    @Override
    public <T> void addFirst(String key, Collection<T> values) {
        this.addFirst(key, values.toArray());
    }

    @Override
    public <T> List<T> getAs(final String key, final Class<T> clazz) {
        return getSubListAs(key, 0, -1, clazz);
    }

    @Override
    public <T> List<T> getSubListAs(final String key, final int fromIndex, final int toIndex, final Class<T> clazz) {
        return template.execute(new RedisCallback<List<T>>() {

            @Override
            public List<T> doWithJedis(Jedis jedis) throws Exception {
                List<T> list = new ArrayList<T>();
                for (String member : jedis.lrange(key, fromIndex, toIndex)) {
                    list.add(RedisObjectCaster.cast(deserialize(member), clazz));
                }
                return list;
            }

        });
    }

    @Override
    public Object get(final String key, final int index) {
        return template.execute(new RedisCallback<Object>() {

            @Override
            public Object doWithJedis(Jedis jedis) throws Exception {
                return deserialize(jedis.lindex(key, index));
            }

        });
    }

    @Override
    public <T> T getAs(final String key, final int index, final Class<T> clazz) {
        return RedisObjectCaster.cast(get(key, index), clazz);
    }

    @Override
    public void removeAt(final String key, final int index) {
        template.execute(new RedisCallback<Object>() {

            @Override
            public Object doWithJedis(Jedis jedis) throws Exception {
                // We don't need to check whether whether DELETED_PLACEHOLDER is unique in the list.
                // DELETED_PLACEHOLDER is always unique because after serialization the value always starts with some prefix - STR:, BOL:, etc.

                // set the element to DELETED_PLACEHOLDER then delete it
                jedis.lset(key, index, DELETED_PLACEHOLDER);
                jedis.lrem(key, 0, DELETED_PLACEHOLDER);
                return null;
            }

        });
    }

    @Override
    public int remove(final String key, final Object member) {
        return template.execute(new RedisCallback<Long>() {

            @Override
            public Long doWithJedis(Jedis jedis) throws Exception {
                return jedis.lrem(key, 0, serialize(member));
            }

        }).intValue();
    }

    @Override
    public int size(final String key) {
        return template.execute(new RedisCallback<Long>() {

            @Override
            public Long doWithJedis(Jedis jedis) throws Exception {
                return jedis.llen(key);
            }

        }).intValue();
    }

    @Override
    public boolean contains(final String key, final Object member) {
        return template.execute(new RedisCallback<Boolean>() {

            @Override
            public Boolean doWithJedis(Jedis jedis) throws Exception {
                List<String> list = jedis.lrange(key, 0, -1);
                return list.contains(serialize(member));
            }

        });
    }

}
