package com.kernelogic.play.redis.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import redis.clients.jedis.Jedis;

import com.kernelogic.play.redis.api.RedisMapManager;
import com.kernelogic.play.redis.internal.RedisCallback;
import com.kernelogic.play.redis.internal.RedisObjectCaster;

/**
 * Implementation of Redis Map operations.
 * 
 * @author Lin Li
 *
 * @since 1.0.0
 */
public class RedisMapManagerImpl extends AbstractRedisManager implements RedisMapManager {

    @Override
    public void put(final String key, final String entryKey, final Object entryValue) {
        template.execute(new RedisCallback<Object>() {

            @Override
            public Object doWithJedis(Jedis jedis) throws Exception {
                jedis.hset(key, entryKey, serialize(entryValue));

                return null;
            }

        });
    }

    @Override
    public void putAll(final String key, final Map<String, Object> map) {
        template.execute(new RedisCallback<Object>() {

            @Override
            public Object doWithJedis(Jedis jedis) throws Exception {
                for (Entry<String, Object> entry : map.entrySet()) {
                    jedis.hset(key, entry.getKey(), serialize(entry.getValue()));
                }

                return null;
            }

        });
    }

    @Override
    public <T> Map<String, T> getAs(final String key, final Class<T> clazz) {
        return template.execute(new RedisCallback<Map<String, T>>() {

            @Override
            public Map<String, T> doWithJedis(Jedis jedis) throws Exception {
                Map<String, String> valueMap = jedis.hgetAll(key);
                Map<String, T> tMap = new HashMap<String, T>();
                for (Entry<String, String> entry : valueMap.entrySet()) {
                    tMap.put(entry.getKey(), RedisObjectCaster.cast(deserialize(entry.getValue()), clazz));
                }

                return tMap;
            }

        });
    }

    @Override
    public Object get(final String key, final String entryKey) {
        return template.execute(new RedisCallback<Object>() {

            @Override
            public Object doWithJedis(Jedis jedis) throws Exception {
                return deserialize(jedis.hget(key, entryKey));
            }

        });
    }

    @Override
    public <T> T getAs(final String key, final String entryKey, final Class<T> clazz) {
        return RedisObjectCaster.cast(get(key, entryKey), clazz);
    }

    @Override
    public <T> Map<String, T> getSubMapAs(final String key, final Class<T> clazz, final String... entryKeys) {
        return template.execute(new RedisCallback<Map<String, T>>() {

            @Override
            public Map<String, T> doWithJedis(Jedis jedis) throws Exception {
                List<String> strList = jedis.hmget(key, entryKeys);
                Map<String, T> map = new HashMap<String, T>();
                for (int i = 0; i < strList.size(); i++) {
                    map.put(entryKeys[i], RedisObjectCaster.cast(deserialize(strList.get(i)), clazz));
                }

                return map;
            }

        });
    }

    @Override
    public <T> Map<String, T> getSubMapAs(String key, Collection<String> entryKeys, Class<T> clazz) {
        return this.getSubMapAs(key, clazz, entryKeys.toArray(new String[0]));
    }

    @Override
    public void remove(final String key, final String... entryKeys) {
        template.execute(new RedisCallback<Object>() {

            @Override
            public Object doWithJedis(Jedis jedis) throws Exception {
                jedis.hdel(key, entryKeys);

                return null;
            }

        });
    }

    @Override
    public void remove(String key, Collection<String> entryKeys) {
        this.remove(key, entryKeys.toArray(new String[0]));
    }

    @Override
    public Set<String> keySet(final String key) {
        return template.execute(new RedisCallback<Set<String>>() {

            @Override
            public Set<String> doWithJedis(Jedis jedis) throws Exception {
                return jedis.hkeys(key);
            }

        });
    }

    @Override
    public List<Object> values(final String key) {
        return template.execute(new RedisCallback<List<Object>>() {

            @Override
            public List<Object> doWithJedis(Jedis jedis) throws Exception {
                List<String> valueList = jedis.hvals(key);
                List<Object> objectList = new ArrayList<Object>();
                for (String value : valueList) {
                    objectList.add(deserialize(value));
                }

                return objectList;
            }

        });
    }

    @Override
    public <T> List<T> valuesAs(final String key, final Class<T> clazz) {
        List<Object> objectList = this.values(key);
        List<T> tList = new ArrayList<T>();
        for (Object object : objectList) {
            tList.add(RedisObjectCaster.cast(object, clazz));
        }

        return tList;
    }

    @Override
    public int size(final String key) {
        return template.execute(new RedisCallback<Long>() {

            @Override
            public Long doWithJedis(Jedis jedis) throws Exception {

                return jedis.hlen(key);
            }

        }).intValue();
    }

    @Override
    public boolean containsKey(final String key, final String entryKey) {
        return template.execute(new RedisCallback<Boolean>() {

            @Override
            public Boolean doWithJedis(Jedis jedis) throws Exception {
                return jedis.hexists(key, entryKey);
            }

        });
    }

}
