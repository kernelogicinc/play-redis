package com.kernelogic.play.redis.impl;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import redis.clients.jedis.Jedis;

import com.kernelogic.play.redis.api.RedisSetManager;
import com.kernelogic.play.redis.internal.RedisCallback;
import com.kernelogic.play.redis.internal.RedisObjectCaster;

/**
 * Implementation of Redis Set operations.
 * 
 * @author Fei Yan
 *
 * @since 1.0.0
 */
public class RedisSetManagerImpl extends AbstractRedisManager implements RedisSetManager {

    @Override
    public void add(final String key, final Object... members) {
        if (members.length == 0) {
            return;
        }

        template.execute(new RedisCallback<Object>() {

            public Object doWithJedis(Jedis jedis) throws Exception {
                jedis.sadd(key, serialize(members));
                return null;
            }

        });
    }

    @Override
    public <T> void add(String key, Collection<T> members) {
        add(key, members.toArray());
    }

    @Override
    public <T> Set<T> getAs(final String key, final Class<T> clazz) {
        return template.execute(new RedisCallback<Set<T>>() {

            public Set<T> doWithJedis(Jedis jedis) throws Exception {
                Set<T> result = new HashSet<>();
                for (String hash : jedis.smembers(key)) {
                    result.add(RedisObjectCaster.cast(deserialize(hash), clazz));
                }
                return result;
            }

        });
    }

    @Override
    public void remove(final String key, final Object... members) {
        if (members.length == 0) {
            return;
        }

        template.execute(new RedisCallback<Object>() {

            public Object doWithJedis(Jedis jedis) throws Exception {
                jedis.srem(key, serialize(members));
                return null;
            }

        });
    }

    @Override
    public <T> void remove(String key, Collection<T> members) {
        remove(key, members.toArray());
    }

    @Override
    public int size(final String key) {
        return template.execute(new RedisCallback<Long>() {

            public Long doWithJedis(Jedis jedis) throws Exception {
                return jedis.scard(key);
            }

        }).intValue();
    }

    @Override
    public boolean contains(final String key, final Object member) {
        return template.execute(new RedisCallback<Boolean>() {

            public Boolean doWithJedis(Jedis jedis) throws Exception {
                return jedis.sismember(key, serialize(member));
            }

        });
    }

}
