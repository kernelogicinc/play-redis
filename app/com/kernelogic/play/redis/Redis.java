package com.kernelogic.play.redis;

import java.util.Collection;
import java.util.Set;

import redis.clients.jedis.JedisPubSub;

import com.kernelogic.play.redis.api.RedisListManager;
import com.kernelogic.play.redis.api.RedisManager;
import com.kernelogic.play.redis.api.RedisMapManager;
import com.kernelogic.play.redis.api.RedisSetManager;
import com.kernelogic.play.redis.impl.RedisListManagerImpl;
import com.kernelogic.play.redis.impl.RedisManagerImpl;
import com.kernelogic.play.redis.impl.RedisMapManagerImpl;
import com.kernelogic.play.redis.impl.RedisSetManagerImpl;

/**
 * Redis Utility.
 * 
 * <p>
 * Please note the Object passed to Redis API must implement {@link java.io.Serializable}, otherwise <code>null</code> will be stored in Redis silently.
 * </p>
 * 
 * @author Peter Fu
 * 
 * @since 1.0.0
 */
public class Redis {

    private static RedisManager redis = new RedisManagerImpl();

    // Make these final so that the implementations cannot be changed by others

    public static final RedisSetManager SET = new RedisSetManagerImpl();

    public static final RedisListManager LIST = new RedisListManagerImpl();

    public static final RedisMapManager MAP = new RedisMapManagerImpl();

    // ---------------------------------------------------
    // Delegate methods from RedisManager
    // ---------------------------------------------------

    /**
     * @param key
     * @param value
     * @see com.kernelogic.play.redis.api.RedisManager#set(java.lang.String, java.lang.Object)
     */
    public static void set(String key, Object value) {
        redis.set(key, value);
    }

    /**
     * @param key
     * @param value
     * @param expire
     * @see com.kernelogic.play.redis.api.RedisManager#set(java.lang.String, java.lang.Object, int)
     */
    public static void set(String key, Object value, int expire) {
        redis.set(key, value, expire);
    }

    /**
     * @param key
     * @return
     * @see com.kernelogic.play.redis.api.RedisManager#get(java.lang.String)
     */
    public static Object get(String key) {
        return redis.get(key);
    }

    /**
     * @param key
     * @param clazz
     * @return
     * @see com.kernelogic.play.redis.api.RedisManager#getAs(java.lang.String, java.lang.Class)
     */
    public static <T> T getAs(String key, Class<T> clazz) {
        return redis.getAs(key, clazz);
    }

    /**
     * @param pattern
     * @return
     * @see com.kernelogic.play.redis.api.RedisManager#keys(java.lang.String)
     */
    public static Set<String> keys(String pattern) {
        return redis.keys(pattern);
    }

    /**
     * @param keys
     * @see com.kernelogic.play.redis.api.RedisManager#remove(java.lang.String[])
     */
    public static void remove(String... keys) {
        redis.remove(keys);
    }

    /**
     * @param keys
     * @see com.kernelogic.play.redis.api.RedisManager#remove(java.util.Collection)
     */
    public static void remove(Collection<String> keys) {
        redis.remove(keys);
    }

    /**
     * @param jedisPubSub
     * @param channels
     * @see com.kernelogic.play.redis.api.RedisManager#subscribe(redis.clients.jedis.JedisPubSub, java.lang.String[])
     */
    public static void subscribe(JedisPubSub jedisPubSub, String... channels) {
        redis.subscribe(jedisPubSub, channels);
    }

    /**
     * @param channel
     * @param message
     * @see com.kernelogic.play.redis.api.RedisManager#publish(java.lang.String, java.lang.String)
     */
    public static void publish(String channel, String message) {
        redis.publish(channel, message);
    }

}
