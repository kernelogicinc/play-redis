package com.kernelogic.play.redis.internal;

import org.apache.commons.codec.binary.Base64;
import org.springframework.core.convert.converter.Converter;
import org.springframework.core.serializer.support.DeserializingConverter;
import org.springframework.core.serializer.support.SerializingConverter;

import play.Logger;

import com.kernelogic.play.redis.internal.spring.ClassLoaderDeserializer;

/**
 * Redis serializer implementation with convenient types.
 * 
 * @author Fei Yan
 * 
 * @since 1.0.0
 */
public class RedisSerializerImpl implements RedisSerializer {

    private static final Logger.ALogger LOGGER = Logger.of(RedisSerializerImpl.class);

    private Converter<Object, byte[]> serializer = new SerializingConverter();

    private Converter<byte[], Object> deserializer = new DeserializingConverter(new ClassLoaderDeserializer());

    @Override
    public Object deserialize(String raw) {
        if (raw == null || raw.equals("NULL")) {
            return null;
        }
        if (raw.startsWith("BOL:")) {
            return raw.substring(4).equals("true");
        }
        if (raw.startsWith("STR:")) {
            return raw.substring(4);
        }
        if (raw.startsWith("INT:")) {
            return new Integer(raw.substring(4));
        }
        if (raw.startsWith("LON:")) {
            return new Long(raw.substring(4));
        }
        int substringStart = 4;// This is for "B64:" prefix
        /*
         * We might also encounter old values from Redis Cache plugin, so we need at least try to deserialize them
         * see https://github.com/typesafehub/play-plugins/blob/master/redis/src/main/scala/com/typesafe/plugin/RedisPlugin.scala#L113
         */
        if (!raw.startsWith("B64:") && raw.contains("-")) {
            substringStart = raw.indexOf("-") + 1;
        }
        byte[] b = Base64.decodeBase64(raw.substring(substringStart).getBytes());

        try {
            return deserializer.convert(b);
        } catch (Exception e) {
            LOGGER.error("Redis deserialize error", e);
        }
        LOGGER.error("Redis deserialize bad input string " + raw);
        return null;
    }

    public String serialize(Object object) {
        if (object == null) {
            return "NULL";
        }
        if (object instanceof Boolean) {
            return "BOL:" + ((Boolean) object ? "true" : "false");
        }
        if (object instanceof String) {
            return "STR:" + object;
        }
        if (object instanceof Integer) {
            return "INT:" + object;
        }
        if (object instanceof Long) {
            return "LON:" + object;
        }

        byte[] b = new byte[0];
        try {
            b = serializer.convert(object);
        } catch (Exception e) {
            LOGGER.error("Redis serialize error for object " + object.toString(), e);
            return "NULL";
        }
        return "B64:" + new String(Base64.encodeBase64(b));
    }

}
