package com.kernelogic.play.redis.internal;

/**
 * Utility to cast an object to another type.
 * 
 * @author Peter Fu
 * 
 * @since 1.0.1.1
 */
public class RedisObjectCaster {

    /**
     * Casts an object to the specified type. Type mismatch among String, Boolean, Integer and Long are tolerated.
     * 
     * @param obj
     * @param clazz
     * @return
     * 
     * @since 1.0.1.1
     */
    public static <T> T cast(Object obj, Class<T> clazz) {
        if (obj == null) {
            return null;
        }

        if (!clazz.isInstance(obj)) {
            if (clazz == Boolean.class) {
                obj = Boolean.valueOf(String.valueOf(obj));
            } else if (clazz == String.class) {
                obj = String.valueOf(obj);
            } else if (clazz == Integer.class) {
                obj = Integer.valueOf(String.valueOf(obj));
            } else if (clazz == Long.class) {
                obj = Long.valueOf(String.valueOf(obj));
            }
        }

        return clazz.cast(obj);
    }

}
