package com.kernelogic.play.redis.internal;

import redis.clients.jedis.Jedis;

/**
 * Callback using the function template defined in RedisTemplate
 * 
 * @author Peter Fu
 *
 * @param <T>
 * 
 * @since 1.0.0
 */
public interface RedisCallback<T> {

    /**
     * Implement the essential operation with the Jedis resource here
     * 
     * @param jedis
     * @return
     * @throws Exception
     */
    T doWithJedis(Jedis jedis) throws Exception;

}
