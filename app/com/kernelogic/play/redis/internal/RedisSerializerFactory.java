package com.kernelogic.play.redis.internal;

/**
 * Redis serializer factory - singleton pattern.
 * 
 * @author Peter Fu
 * 
 * @since 1.0.0
 */
public class RedisSerializerFactory {

    private static final RedisSerializer SERIALIZER = new RedisSerializerImpl();

    /**
     * Gets the singleton serializer instance.
     * 
     * @return
     * 
     * @since 1.0.0
     */
    public static RedisSerializer get() {
        return SERIALIZER;
    }

}
