package com.kernelogic.play.redis.internal;

/**
 * Redis serializer.
 * 
 * <p>
 * For basic data type such as String and Integer, the implementation makes them readable in Redis to easy debug.
 * </p>
 * 
 * <p>
 * For custom data type - normal java objects, the implementation serialize them to use Base64 to encode.
 * </p>
 * 
 * @author Peter Fu
 * 
 * @since 1.0.0
 */
public interface RedisSerializer {

    Object deserialize(String raw);

    String serialize(Object object);

}
