package com.kernelogic.play.redis.internal.spring;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectStreamClass;

import play.Play;

/**
 * To dynamically load classes from libraries (or even sub project?) in Play run mode, need to use Play's class loader.
 * 
 * <p>
 * Idea comes from https://github.com/typesafehub/play-plugins/blob/redis-2.2.1/redis/src/main/scala/com/typesafe/plugin/RedisPlugin.scala#L164.
 * </p>
 * 
 * <p>
 * It seems that in Play run mode, libraries classes and application classes are in different class loaders. More discussions here
 * https://github.com/playframework/playframework/issues/2847.
 * </p>
 * 
 * @author Peter Fu
 * 
 * @since 1.0.0
 */
public class ClassLoaderObjectInputStream extends ObjectInputStream {

    public ClassLoaderObjectInputStream(InputStream in) throws IOException {
        super(in);
    }

    @Override
    protected Class<?> resolveClass(ObjectStreamClass desc) throws IOException, ClassNotFoundException {
        return Class.forName(desc.getName(), false, Play.application().classloader());
    }

}
