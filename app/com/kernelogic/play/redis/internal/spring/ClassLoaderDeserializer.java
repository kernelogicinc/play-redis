package com.kernelogic.play.redis.internal.spring;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;

import org.springframework.core.NestedIOException;
import org.springframework.core.serializer.Deserializer;

/**
 * Implementation of Spring {@link Deserializer} using {@link ClassLoaderObjectInputStream}.
 * 
 * @author Peter Fu
 * 
 * @since 1.0.0
 */
public class ClassLoaderDeserializer implements Deserializer<Object> {

    @Override
    public Object deserialize(InputStream inputStream) throws IOException {
        ObjectInputStream objectInputStream = new ClassLoaderObjectInputStream(inputStream);
        try {
            return objectInputStream.readObject();
        } catch (ClassNotFoundException ex) {
            throw new NestedIOException("Failed to deserialize object type", ex);
        } finally {
            objectInputStream.close();
        }
    }

}
