package com.kernelogic.play.redis.internal;

import play.Play;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.exceptions.JedisException;

/**
 * Template of Redis operation based on Jedis.
 * 
 * @author Peter Fu
 * 
 * @since 1.0.0
 */
public class RedisTemplate {

    private static final RedisTemplate TEMPLATE = new RedisTemplate();

    /**
     * Gets the singleton template instance.
     * 
     * @return
     * 
     * @since 1.0.0
     */
    public static RedisTemplate get() {
        return TEMPLATE;
    }

    /**
     * Acquires a Jedis resource from the pool.
     * 
     * @return
     * 
     * @since 1.0.0
     */
    Jedis acquireResource() {
        return Play.application().injector().instanceOf(JedisPool.class).getResource();
    }

    /**
     * Returns the Jedis resource to the pool.
     * 
     * @param jedis
     * 
     * @since 1.0.0
     */
    void returnResrouce(Jedis jedis) {
        Play.application().injector().instanceOf(JedisPool.class).returnResource(jedis);
    }

    /**
     * Executes a RedisCallback, always return the Jedis resource after the operation.
     * 
     * @param callback
     * @return
     * 
     * @since 1.0.0
     */
    public <V> V execute(RedisCallback<V> callback) {
        return execute(callback, true);
    }

    /**
     * Executes a RedisCallback.
     * 
     * <p>
     * TODO There is no obvious evidence for the need to prevent it from returning the resource after Jedis.subscribe method is completed. So I made this method private and later
     * on I might consider to remove the boolean flag returnResource when I find more about it. - by Peter on Sep 10, 2014
     * </p>
     * 
     * @param callback
     * @param returnResource
     * @return
     * 
     * @since 1.0.0
     */
    private <V> V execute(RedisCallback<V> callback, boolean returnResource) {
        Jedis jedis = acquireResource();
        V result = null;

        try {
            result = callback.doWithJedis(jedis);
        } catch (JedisException e) {
            throw new RuntimeException("Jedis operation error", e);
        } catch (Exception e) {
            throw new RuntimeException("Redis operation error", e);
        } finally {
            if (returnResource) {
                returnResrouce(jedis);
            }
        }

        return result;
    }

}
