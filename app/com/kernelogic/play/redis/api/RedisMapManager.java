package com.kernelogic.play.redis.api;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Interface of Redis Map (Hash) operations.
 * 
 * @author Peter Fu
 * 
 * @since 1.0.0
 */
public interface RedisMapManager {

    /**
     * Sets the specified entry key field to the specified entry value for the map stored at key.
     * 
     * @param key
     * @param entryKey
     * @param entryValue
     * 
     * @since 1.0.0
     */
    void put(String key, String entryKey, Object entryValue);

    /**
     * Sets the respective entry keys to the respective entry values for the map stored at key. It replaces old values with new values.
     * 
     * @param key
     * @param map
     * 
     * @since 1.0.0
     */
    void putAll(String key, Map<String, Object> map);

    /**
     * Returns all the entries in the map stored at key with type safety.
     * 
     * @param key
     * @param clazz
     * @return
     * 
     * @since 1.0.0
     */
    <T> Map<String, T> getAs(String key, Class<T> clazz);

    /**
     * Retrieves the entry value associated to the specified entry key from the map stored at key.
     * 
     * @param key
     * @param entryKey
     * @return
     * 
     * @since 1.0.0
     */
    Object get(String key, String entryKey);

    /**
     * Retrieves the entry value associated to the specified entry key from the map stored at key with type safety.
     * 
     * @param key
     * @param entryKey
     * @param clazz
     * @return
     * 
     * @since 1.0.0
     */
    <T> T getAs(String key, String entryKey, Class<T> clazz);

    /**
     * Retrieves the entry values associated to the specified entry keys from the map stored at key with type safety.
     * 
     * <p>
     * This method is an exception of the parameter convention of getXAs methods, because clazz is not the last parameter here. The reason is that entryKeys must be the last
     * parameter as required by JDK.
     * </p>
     * 
     * @param key
     * @param clazz
     * @param entryKeys
     * @return
     * 
     * @since 1.0.0
     */
    <T> Map<String, T> getSubMapAs(String key, Class<T> clazz, String... entryKeys);

    /**
     * Retrieves the entry values associated to the specified entry keys from the map stored at key with type safety.
     * 
     * @param key
     * @param entryKeys
     * @param clazz
     * @return
     * 
     * @since 1.0.0
     */
    <T> Map<String, T> getSubMapAs(String key, Collection<String> entryKeys, Class<T> clazz);

    /**
     * Removes the specified entries from the map stored at key.
     * 
     * @param key
     * @param entryKeys
     * 
     * @since 1.0.0
     */
    void remove(String key, String... entryKeys);

    /**
     * Removes the specified entries from the map stored at key.
     * 
     * @param key
     * @param entryKeys
     * 
     * @since 1.0.0
     */
    void remove(String key, Collection<String> entryKeys);

    /**
     * Returns all the fields in the map stored at key.
     * 
     * @param key
     * @return
     * 
     * @since 1.0.0
     */
    Set<String> keySet(String key);

    /**
     * Returns all the values in the map stored at key.
     * 
     * @param key
     * @return
     * 
     * @since 1.0.0
     */
    List<Object> values(String key);

    /**
     * Returns all the values in the map stored at key with type safety.
     * 
     * @param key
     * @param clazz
     * 
     * @since 1.0.0
     */
    <T> List<T> valuesAs(String key, Class<T> clazz);

    /**
     * Return the number of entries in the map stored at key.
     * 
     * @param key
     * @return
     * 
     * @since 1.0.0
     */
    int size(String key);

    /**
     * Tests for existence of a specified entry key in the map stored at key.
     * 
     * @param key
     * @param entryKey
     * @return
     * 
     * @since 1.0.0
     */
    boolean containsKey(String key, String entryKey);

}
