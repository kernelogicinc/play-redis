package com.kernelogic.play.redis.api;

import java.util.Collection;
import java.util.Set;

import redis.clients.jedis.JedisPubSub;

/**
 * Interface of Redis common operations.
 * 
 * @author Peter Fu
 * 
 * @since 1.0.0
 */
public interface RedisManager {

    /**
     * Saves a value for the key.
     * 
     * @param key
     * @param value
     * 
     * @since 1.0.0
     */
    void set(String key, Object value);

    /**
     * Saves a value for the key with expiration in seconds.
     * 
     * @param key
     * @param value
     * @param expire
     * 
     * @since 1.0.0
     */
    void set(String key, Object value, int expire);

    /**
     * Gets a value for the key.
     * 
     * <p>
     * Since the returned value is not type safe, so most of time please use {@link RedisManager#getAs(String, Class)} instead to retreive a value with type safety.
     * </p>
     * 
     * @param key
     * @return
     * 
     * @see RedisManager#getAs(String, Class)
     * @since 1.0.0
     */
    Object get(String key);

    /**
     * Gets a value for the key with type safety.
     * 
     * @param key
     * @param clazz
     * @return
     * 
     * @since 1.0.0
     */
    <T> T getAs(String key, Class<T> clazz);

    /**
     * Finds keys which match the pattern.
     * 
     * @param pattern
     * @return
     * 
     * @since 1.0.0
     */
    Set<String> keys(String pattern);

    /**
     * Removes the keys.
     * 
     * <p>
     * Regex not supported here.
     * </p>
     * 
     * @param keys Can be an empty list, but can't be null
     * 
     * @since 1.0.0
     */
    void remove(String... keys);

    /**
     * Removes the keys.
     * 
     * <p>
     * Regex not supported here.
     * </p>
     * 
     * @param keys Can be an empty collection, but can't be null
     * 
     * @since 1.0.0
     */
    void remove(Collection<String> keys);

    /**
     * Subscribes to channels.
     * 
     * <p>
     * Note calling to this method will block current thread, make sure a dedicated thread is created for subscribe.
     * </p>
     * 
     * @param jedisPubSub
     * @param channels
     * 
     * @since 1.0.0
     */
    void subscribe(JedisPubSub jedisPubSub, String... channels);

    /**
     * Publishes a message to a channel.
     * 
     * @param channel
     * @param message
     * 
     * @since 1.0.0
     */
    void publish(String channel, String message);

}
