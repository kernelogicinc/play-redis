package com.kernelogic.play.redis.api;

import java.util.Collection;
import java.util.Set;

/**
 * Interface of Redis Set operations.
 * 
 * @author Peter Fu
 * 
 * @since 1.0.0
 */
public interface RedisSetManager {

    /**
     * Adds the specified members to the set stored at key.
     * 
     * @param key
     * @param members
     * 
     * @since 1.0.0
     */
    void add(String key, Object... members);

    /**
     * Adds the specified members to the set stored at key.
     * 
     * @param key
     * @param members
     * 
     * @since 1.0.0
     */
    <T> void add(String key, Collection<T> members);

    /**
     * Returns all the members of the set value stored at key with type safety.
     * 
     * @param key
     * @param clazz
     * @return
     * 
     * @since 1.0.0
     */
    <T> Set<T> getAs(String key, Class<T> clazz);

    /**
     * Removes the specified members from the set stored at key.
     * 
     * @param key
     * @param members
     * 
     * @since 1.0.0
     */
    void remove(String key, Object... members);

    /**
     * Removes the specified members from the set stored at key.
     * 
     * @param key
     * @param members
     * 
     * @since 1.0.0
     */
    <T> void remove(String key, Collection<T> members);

    /**
     * Returns the set cardinality (number of elements).
     * 
     * <p>
     * If the key does not exist 0 is returned, like for empty sets.
     * </p>
     * 
     * @param key
     * @return
     * 
     * @since 1.0.0
     */
    int size(String key);

    /**
     * Returns true if the set stored at key contains the specified member, otherwise false is returned.
     * 
     * @param key
     * @param member
     * @return
     * 
     * @since 1.0.0
     */
    boolean contains(String key, Object member);

}
