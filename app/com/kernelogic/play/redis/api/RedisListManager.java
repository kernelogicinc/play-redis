package com.kernelogic.play.redis.api;

import java.util.Collection;
import java.util.List;

/**
 * Interface of Redis List operations.
 * 
 * @author Peter Fu
 * 
 * @since 1.0.0
 */
public interface RedisListManager {

    /**
     * Add the values to the tail of the list stored at key.
     * 
     * <p>
     * This method is equivalent to {@link #addLast(String, Object...)}.
     * </p>
     * 
     * @param key
     * @param values
     * 
     * @since 1.0.0
     */
    void add(String key, Object... values);

    /**
     * Add the values to the tail of the list stored at key.
     * 
     * <p>
     * If the key does not exist an empty list is created just before the append operation.
     * </p>
     * 
     * @param key
     * @param values
     * 
     * @since 1.0.0
     */
    void addLast(String key, Object... values);

    /**
     * Add the values to the head of the list stored at key.
     * 
     * <p>
     * If the key does not exist an empty list is created just before the append operation.
     * </p>
     * 
     * <p>
     * Note that if more than one values are passed in to this method, they will be added to head of the list one by one, which will result in reverse order. For example
     * {@code addFirst("somekey", "foo", "bar", "baz")} will result in the following order in the list stored in Redis: {@code "baz", "bar", "foo"}. This behavior is the same with
     * Jedis and Redis.
     * </p>
     * 
     * @param key
     * @param values
     * 
     * @since 1.0.0
     */
    void addFirst(String key, Object... values);

    /**
     * Add the values to the tail of the list stored at key.
     * 
     * @param key
     * @param values
     * 
     * @see #add(String, Object...)
     * 
     * @since 1.0.0
     */
    <T> void add(String key, Collection<T> values);

    /**
     * Add the values to the tail of the list stored at key.
     * 
     * @param key
     * @param values
     * 
     * @see #addLast(String, Object...)
     * 
     * @since 1.0.0
     */
    <T> void addLast(String key, Collection<T> values);

    /**
     * Add the values to the head of the list stored at key.
     * 
     * @param key
     * @param values
     * 
     * @see #addFirst(String, Object...)
     * 
     * @since 1.0.0
     */
    <T> void addFirst(String key, Collection<T> values);

    /**
     * Return all elements of the list stored at the specified key with type safety.
     * 
     * @param key
     * @param clazz
     * @return
     * 
     * @since 1.0.0
     */
    <T> List<T> getAs(String key, Class<T> clazz);

    /**
     * Returns a view of the portion of the list stored at the specified key with type safety between the specified <tt>fromIndex</tt>, inclusive, and <tt>toIndex</tt>, inclusive.
     * 
     * <p>
     * <tt>toIndex</tt> is made inclusive intentionally to be consistent with Jedis API, althought it's inconsistent with JDK List API now.
     * </p>
     * 
     * @param key
     * @param clazz
     * @return
     * 
     * @since 1.0.0
     */
    <T> List<T> getSubListAs(String key, int fromIndex, int toIndex, Class<T> clazz);

    /**
     * Returns the specified member of the list stored at the specified key.
     * 
     * @param key
     * @param index
     * @return
     * 
     * @since 1.0.0
     */
    Object get(String key, int index);

    /**
     * Returns the specified member of the list stored at the specified key with type safety.
     * 
     * @param key
     * @param index
     * @param clazz
     * @return
     * 
     * @since 1.0.0
     */
    <T> T getAs(String key, int index, Class<T> clazz);

    /**
     * Removes the member at the specified index from the list stored at key.
     * 
     * <p>
     * There is no equivalent Redis operation for this, so this is actually a shortcut of the following statements (fake code).
     * </p>
     * 
     * <pre>
     * set(key, index, &quot;-- deleted --&quot;);
     * remove(key, &quot;-- deleted --&quot;);
     * </pre>
     * 
     * <p>
     * The implementation should make sure the placeholder for the member to be deleted is unique in the list, which introduces extra complexity. So if possible use
     * {@code #remove(String, Object)} instead which has better performance.
     * </p>
     * 
     * @param key
     * @param index
     * 
     * @see #remove(String, Object)
     * 
     * @since 1.0.1
     */
    void removeAt(String key, int index);

    /**
     * Removes the specified member at the list stored at key.
     * 
     * @param key
     * @param member
     * @return The count of removed members.
     * 
     * @since 1.0.0
     */
    int remove(String key, Object member);

    /**
     * Return the length of the list stored at the specified key.
     * 
     * <p>
     * If the key does not exist zero is returned (the same behavior as for empty lists).
     * </p>
     * 
     * @param key
     * @return
     * 
     * @since 1.0.0
     */
    int size(String key);

    /**
     * Returns true if the list stored at key contains the specified member, otherwise false is returned.
     *
     * <p>
     * There is no equivalent Redis operation for this, always need to retrieve all the list to check whether it contains the specified member.
     * </p>
     * 
     * @param key
     * @param member
     * @return
     * 
     * @since 1.0.0
     */
    boolean contains(String key, Object member);

}
